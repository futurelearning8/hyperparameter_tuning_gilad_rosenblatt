import numpy as np
import pandas as pd
from scipy import stats
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, precision_score, accuracy_score


class Pipeline:
    """
    Pipeline to read > clean > split > normalize > fit > score a scikit-learn syntax-compatible model.
    """

    # Path to data.
    filename = "../data/weatherAUS.csv"

    # Target column names.
    target = ["RainTomorrow"]

    # Feature column names: UPDATES with feature variables during clean.
    features = []

    def __init__(self, score_metric=accuracy_score, model_factory=LogisticRegression):
        """Instantiate a new pipeline for the named model."""

        # Factory method for the model to instantiate from.
        self.model_factory = model_factory

        # Score method given y and y_preds to evaluate model performance.
        self._score = score_metric

        # Sklearn-syntax compatible model object (will-be after calling fit).
        self.model = None

    @staticmethod
    def read():
        return pd.read_csv(Pipeline.filename)

    @classmethod
    def clean(cls, df):

        # Drop "irrelevant" or data leaking (RISK_MM) columns.
        # Read about the dataset here: <https://www.kaggle.com/jsphyg/weather-dataset-rattle-package>
        # Read about RISK_MM here: <https://www.kaggle.com/jsphyg/weather-dataset-rattle-package/discussion/78316>
        cols_drop = ["Sunshine", "Evaporation", "Cloud3pm", "Cloud9am", "Location", 'RISK_MM', "Date"]
        df = df.drop(columns=cols_drop)

        # Drop rows for which any of the features or target is missing and shuffle in place.
        df = df\
            .dropna(how="any")\
            .sample(frac=1, random_state=42)

        # Remove outliers in the data based on z-scores.
        z_score = np.abs(stats.zscore(df._get_numeric_data()))
        df = df[(z_score < 3).all(axis=1)]

        # Change yes/no to 1/0 for RainToday and RainTomorrow.
        df["RainToday"].replace({"No": 0, "Yes": 1}, inplace=True)
        df["RainTomorrow"].replace({"No": 0, "Yes": 1}, inplace=True)

        # Convert unique categorical column values to int using dummy variables.
        cols_categorical = ["WindGustDir", "WindDir3pm", "WindDir9am"]
        df = pd.get_dummies(df, columns=cols_categorical)

        # Keep remaining columns as features.
        cls.features = [col for col in df.columns if col not in Pipeline.target]

        # Extract numpy arrays for regression.
        X = df[Pipeline.features].values
        y = df[Pipeline.target].values

        # Return features and targets.
        return X, y

    @staticmethod
    def split(X, y):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        return X_train, X_test, y_train, y_test

    @staticmethod
    def normalize(X_train, X_test, y_train, y_test):
        scaler = StandardScaler()
        scaler.fit(X_train)
        return scaler.transform(X_train), scaler.transform(X_test), y_train, y_test

    def fit(self, X_train, X_test, y_train, y_test, *args, **kwargs):
        self.model = self.model_factory(*args, **kwargs)
        self.model.fit(X_train, y_train.reshape(-1, ))
        return self.model.predict(X_train), self.model.predict(X_test), y_train, y_test

    def score(self, y_train_preds, y_test_preds, y_train, y_test):
        return self._score(y_train, y_train_preds), self._score(y_test, y_test_preds)

    def run(self, *args, **kwargs):
        """Runs the pipeline read + shuffle > clean + select features > split > normalize > fit > score."""
        train_score, test_score = \
            self.score(
                *self.fit(
                    *Pipeline.normalize(
                        *Pipeline.split(
                            *Pipeline.clean(
                                Pipeline.read()
                            )
                        )
                    ),
                    *args,
                    **kwargs
                )
            )
        return train_score, test_score

    @property
    def coefficients(self):
        theta = np.append(
            self.model.intercept_.reshape((-1, 1)),
            self.model.coef_, axis=1
        )
        return theta

    def explain_model(self):
        print(f"---------------------- Model Explanation ------------------------")
        print(f"Model: {self.model.__class__.__name__}")
        print(f"Targets: {', '.join(Pipeline.target)}.")
        print(f"Features: {', '.join(Pipeline.features)}.")
        print(f"-----------------------------------------------------------------")

    def report_score(self, y, y_preds):
        self.print_score(self.model, self._score, self._score(y, y_preds))

    @staticmethod
    def print_score(model, metric, score, set_name=None, hyperparameter_dict=None):
        """
        Utility method to print scores for a model.

        :param model: the model instance to which the score belongs.
        :param collections.Callable metric: metric function of the scalar = function(y, y_preds).
        :param float score: metric score to print.
        :param str set_name: name of set for which score was obtained (e.g., "train", "validation", "test").
        :param dict hyperparameter_dict: hyperparameter names and values for which score was obtained on this model.
        """
        print(f"------------------------- SCORE ---------------------------------")
        print(f"Model: {model.__class__.__name__}")
        if hyperparameter_dict:
            for name, value in hyperparameter_dict.items():
                print(f"Hyperparameter: {name} = {value}")
        print(f"Metric: {metric.__name__}")
        if set_name:
            print(f"Set: {set_name}")
        print(f"Value: {score:.6f}")
        print(f"-----------------------------------------------------------------")

    @staticmethod
    def print_classification_report(y_test, y_test_preds, source):
        print(f"-----------------------------------------------------------------")
        print(f"Evaluation for {source} on test set:")
        print(f"-----------------------------------------------------------------")
        print(classification_report(y_test, y_test_preds))
        print(f"-----------------------------------------------------------------")


def main():
    """Run a pipeline to read > clean > split > normalize > fit > score a linear model using scikit-learn."""
    pipeline = Pipeline(
        score_metric=precision_score,
        model_factory=LogisticRegression
    )
    train_score, test_score = pipeline.run()
    Pipeline.print_score(
        model=pipeline.model,
        metric=precision_score,
        score=test_score,
        set_name="test"
    )


if __name__ == '__main__':
    main()
