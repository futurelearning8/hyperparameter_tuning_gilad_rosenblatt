import seaborn as sns
import matplotlib.pyplot as plt
from pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from logistic_regression import LogisticRegression


def main():
    # Load the dataset, shuffle, clean, split, and normalize (StandardScaler) using the Pipeline class.
    X_train, X_test, y_train, y_test = \
        Pipeline.normalize(
            *Pipeline.split(
                *Pipeline.clean(
                    Pipeline.read()
                )
            )
        )

    # Fit the model on the training set (validation set is provided only to evaluate metrics).
    optimizer = LogisticRegression(
        regularize=True,
        momentum=True,
        reduce_on_plateau=True,
        verbose=True,
        plot=True
    ).fit(X_train, y_train, X_test, y_test)

    # Score the model on the train and test sets (use probability metric threshold of 0.5).
    custom_cm = (
        confusion_matrix(y_train, optimizer.predict(X_train)),
        confusion_matrix(y_test, optimizer.predict(X_test))
    )

    # Now solve the same optimization problem using a scikit-learn LinearRegression model (implemented in Pipeline).
    pipeline = Pipeline(score_metric=confusion_matrix)
    sklearn_cm = pipeline.run()

    # Print comparison between model outcomes to console.
    pipeline.explain_model()
    Pipeline.print_classification_report(
        y_test,
        pipeline.model.predict(X_test),
        source="scikit-learn LogisticRegression"
    )
    Pipeline.print_classification_report(
        y_test,
        optimizer.predict(X_test),
        source="custom LogisticRegression"
    )

    # Confusion matrix: plot comparison between model outcomes.
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10, 7), num="Confusion matrices")
    for axis_row, model_cm, model_name in zip(axes, (custom_cm, sklearn_cm), ("custom", "scikit-learn")):
        for ax, cm, set_name in zip(axis_row, model_cm, ("train", "text")):
            sns.heatmap(cm, ax=ax, annot=True, fmt="d", cbar=False)
            ax.set_title(f"{model_name.capitalize()} model: {set_name} set")
    plt.show()
    # MANUALLY CLOSE FIGURES TO FINISH PROCESS.


if __name__ == '__main__':
    main()
