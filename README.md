# Hyperparameter Tuning

This is the hyperparameter tuning exercise for FL8 week 2 day 2 by Gilad Rosenblatt.

## Instructions

### Run

Run `main.py` in the terminal from `code` as current working directory for section 1-9 (building the optimizer).

Run `tuning.py` in the terminal from `code` as current working directory for section 10 (hyper parameter tuning).

### Data

Data is not included in this repo. The ```data``` directory should in the repo base directory,
one level up the tree from `code`. The path to the data file is assumed to be `data/weatherAUS.csv`. The dataset can be found [here](https://www.kaggle.com/jsphyg/weather-dataset-rattle-package).

## License

[WTFPL](http://www.wtfpl.net/)
