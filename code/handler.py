from abc import ABC, abstractmethod


class Handler(ABC):
    """An interface for a handler to monitor learning rate and reduce it on plateau."""

    @abstractmethod
    def update(self, new_loss):
        """Update learning rate according to new loss value."""
        pass

    @abstractmethod
    def get_learning_rate(self):
        """Return most updated learning rate value."""
        pass


class LearningRateHandler(Handler):
    """A handler to monitor learning rate and reduce it on plateau."""

    def __init__(self, learning_rate, factor=0.2, patience=5, min_learning_rate=0.001, verbose=False):
        """
        Initialize a learning rate handler with the initial value of the learning rate.

        :param float learning_rate: initial value of learning rate (at epoch 1).
        :param float factor: factor by which the learning rate will be reduced.
        :param int patience: number of epochs with no improvement after which learning rate will be reduced.
        :param float min_learning_rate: lower bound on the learning rate.
        :param bool verbose: False is quiet True means update messages.
        """

        # Save the initial (default) learning rate.
        self._learning_rate = learning_rate

        # Save handler parameters:
        self._factor = factor
        self._patience = patience
        self._min_learning_rate = min_learning_rate
        self._verbose = verbose

        # Save non-input decision parameters: significance level to update and number of cooldown cycles after update.
        self._significance = 0.1
        self._cooldown_period = 5
        self._epochs_since_update = 0

        # Initialize loss history (to at least remember last "patience" epochs).
        # TODO use O(1) memory with a rotating list pf "patience" elements.
        self._loss_history = []

    def update(self, new_loss):
        """
        Update learning rate according to new cost value.

        :param float new_loss: updated value of loss for which the learning rate needs to be adjusted.
        """

        # Save new loss to history.
        self._loss_history.append(new_loss)

        # Increment epochs-since-update counter.
        self._epochs_since_update += 1

        # Extract loss for the last "patience" epochs.
        recent_losses = self._loss_history[-self._patience:]

        # If loss is stagnate over the last "patience" epochs - and cooldown period has elapsed - flag for update.
        loss_mean = sum(recent_losses) / len(recent_losses)
        loss_range = abs(max(recent_losses) - min(recent_losses))
        change = loss_range / loss_mean
        if self._epochs_since_update >= self._cooldown_period and change < self._significance:
            update_now = True
        else:
            update_now = False

        # If flagged for update: update learning rate if new rate is above minimum allowed value.
        if update_now and self._factor * self._learning_rate >= self._min_learning_rate:

            # Update learning rate (to be extracted via explicit call to get_learning_rate)
            self._learning_rate = self._factor * self._learning_rate

            # Nullify epochs-since-update counter.
            self._epochs_since_update = 0

            # Print to console if in verbose mode:
            if self._verbose:
                print(f"--------------------------- Handler -----------------------------")
                print(f"Updated learning rate to {self._learning_rate}.")
                print(f"-----------------------------------------------------------------")

    def get_learning_rate(self):
        """Return most updated learning rate value."""
        return self._learning_rate
