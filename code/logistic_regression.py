"""
Logistic regression using batch gradient descent with momentum by Gilad Rosenblatt.
Supports regularization and learning rate policy adjustment on plateau.

NOTE: this implementation is fully-vectorized (up to epoch/batch iterations) and performs binary classification.
In the binary classification (1-class) m-features and n-samples matrix dimensions are as follows:

    Dimension of X (features matrix):

             feature 1   feature 2 ... feature m
    sample 1    x           x             x
    sample 2    x           x             x
    .
    .
    .
    sample n    x           x             x

    Dimension of y (class label vector):

             class 1
    sample 1    x
    sample 2    x
    .
    .
    .
    sample n    x

    Dimension of theta (model coefficients vector):

             feature 1   feature 2 ... feature m
    class 1    x           x             x
"""

import numpy as np
from abc import ABC, abstractmethod
from utils import RealTimePlotter
from handler import LearningRateHandler


# TODO plot at each batch


class _Hyperparameters:

    def __init__(self, learning_rate=0.3, batch_size=3000, epochs=80, threshold=0.5, lambda_factor=3, beta_factor=0.1):
        """
        Initialize a container of hyperparameters for a regularized batch gradient descent with momentum optimizer.

        :param float learning_rate: value of the learning rate (at epoch 1, real value can be dynamic).
        :param int batch_size: number of samples in a batch for batch gradient descent.
        :param int epochs: number of epochs for which to run the training.
        :param float threshold: classification threshold between 0 and 1 from probability measures to classes.
        :param float lambda_factor: positive regularization factor.
        :param float beta_factor: positive momentum factor.
        """

        # Learning rate.
        self.learning_rate = learning_rate

        # Training batch size per iteration.
        self.batch_size = batch_size

        # Total number of passes through the data.
        self.epochs = epochs

        # Prediction threshold.
        self.threshold = threshold

        # Regularization parameter.
        self.lambda_factor = lambda_factor

        # Momentum parameter.
        self.beta_factor = beta_factor

    def to_dictionary(self):
        """Returns the class dict (containing instance attributes and values)."""
        return self.__dict__


class Model(ABC):
    """Optimizable model."""

    @abstractmethod
    def fit(self, X, y):
        """
        Fit model to sample set and target value.

        :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
        :param np.ndarray y: class labels vector (ground truth for each sample).
        """

        pass

    @abstractmethod
    def predict(self, X):
        """
        Predict target value on sample set using trained model.

        :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
        """
        pass


class LogisticRegression(Model):
    """Logistic regression model for binary classification based on a batch gradient descent optimizer."""

    def __init__(self, regularize=False, momentum=False, reduce_on_plateau=False, verbose=False, plot=False, **kwargs):
        """
        Instantiate a custom logistic regression model trained using regularized batch gradient descent with momentum
        and an adaptive learning rate. Hyper parameters are contained in a container named hyper.

        :param bool regularize: if True add regularization tern to cost function to avoid overfitting.
        :param bool momentum: if True adds momentum to gradients at each step using exponential moving average.
        :param bool reduce_on_plateau: if True learning rate is changes dynamically to avoid getting stuck pn a plateau.
        :param bool verbose: id True prints updates to screen at each epoch with loss parameters.
        :param bool plot: if True plots learning curves at each epoch in real-time.
        """
        # Instantiate hyperparameter container.
        self.hyper = _Hyperparameters(**kwargs)

        # Save regularization state: if True add regularization term to cost function and gradient.
        self.regularize = regularize

        # Save momentum state: if True gradients will use an exponential moving average recursive update rule.
        self.momentum = momentum

        # Save reduce_on_plateau state: if True the learning rate will be reduces on plateau (if cost does not change).
        self.reduce_on_plateau = reduce_on_plateau

        # Save reporting state: if True print state during optimization to console.
        self.verbose = verbose

        # Set the plot-metrics flag (plots learning curves during optimization if True).
        self.plot = plot

        # Initialize model cost function values on train and validation sets..
        self.cost_train = None
        self.cost_validation = None

        # Initialize model coefficients state variable.
        self._theta = None

        # Instantiate training set placeholders.
        self._X = None
        self._y = None

        # Instantiate validation set placeholders (for model tuning and evaluation).
        self._compare_to_validation_set = False
        self._X_val = None
        self._y_val = None

        # Instantiate state variable for the number of epochs elapsed during optimization.
        self._epoch_number = None

        # Instantiate last gradient update state parameter (for momentum)
        self._previous_grad = 0

        # Instantiate handler to monitor validation cost (feeds updated value of learning rate at each epoch).
        self._handler = LearningRateHandler(
            learning_rate=self.hyper.learning_rate,
            verbose=self.verbose
        )

    @staticmethod
    def _sigmoid(Z):
        """
        :param np.ndarray Z: input to the sigmoid function.
        :return np.ndarray: the sigmoid (inverse logit) of the input.
        """
        return 1 / (1 + np.exp(-Z))

    def _hypothesis(self, X):
        """
        :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
        :return np.ndarray: model's probability measure on the sample set.
        """
        return LogisticRegression._sigmoid(np.matmul(X, self._theta.T))

    def _cost(self, X, y):
        """
        :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
        :param np.ndarray y: class labels vector (ground truth for each sample).
        :return np.ndarray: the model's cross entropy loss over all samples (rows-1, cols-classes).
        """

        # Calculate regularization term.
        if self.regularize:
            regularization = self.hyper.lambda_factor * np.linalg.norm(self._theta) ** 2 / 2
        else:
            regularization = 0

        # Calculate cost function.
        h = self._hypothesis(X)
        y = y.reshape(-1, 1)
        return -np.mean(y * np.log(h) + (1 - y) * np.log(1 - h)) + regularization

    def _gradient(self, X, y):
        """
        :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
        :param np.ndarray y: class labels vector (ground truth for each sample).
        :return np.ndarray gradient: coefficients gradients for current step.
        """

        # Calculate regularization term.
        if self.regularize:
            regularization = self.hyper.lambda_factor * self._theta / self._theta.shape[1]
        else:
            regularization = 0

        # Calculate gradient.
        h = self._hypothesis(X)
        this_grad = np.matmul((h - y.reshape(-1, 1)).T, X) / X.shape[0] + regularization

        # Update using momentum (exponential moving average update rule).
        grad = self.hyper.beta_factor * self._previous_grad + (1 - self.hyper.beta_factor) * this_grad
        self._previous_grad = grad

        # Return gradient.
        return grad

    def _run_batch_gradient_descent(self):
        """ Run batch gradient decent to optimized model coefficients."""

        # Randomly initialize theta (model coefficients).
        num_cols = self._X.shape[1]  # bias + number of features
        num_rows = self._y.shape[1]  # number of classes
        self._theta = np.random.random((num_rows, num_cols))

        # Determine how many steps are needed per epoch.
        num_samples = self._X.shape[0]
        num_steps = num_samples // self.hyper.batch_size + 1

        # Start a loop over the number of epochs (start counting epochs from 1).
        self._epoch_number = 0
        for epoch_num in range(self.hyper.epochs):
            self._epoch_number += 1  # On the last iteration epoch_number == hyper.epochs

            # Start a loop over the number of steps required to finish an epoch
            for step_num in range(num_steps):
                # Fetch the next batch from the data (finish above range cuts slice without raising an exception).
                start = step_num * self.hyper.batch_size
                finish = start + self.hyper.batch_size

                # Obtain updated learning rate from learning rate handler.
                this_learning_rate = self._handler.get_learning_rate()

                # Compute the gradient for this batch and update model coefficients according to learning rate.
                self._theta -= this_learning_rate * self._gradient(
                    self._X[start:finish],
                    self._y[start:finish]
                )

            # Update metrics being tracked (like cost value for current state model).
            self._update_metrics()

            # Report metrics for this epoch if in verbose mode.
            if self.verbose:
                self._report_metrics()

            # Plot metrics for this epoch if in plot mode.
            if self.plot:
                self._plot_metrics()

    def _update_metrics(self):
        """Update metrics like value of cost function for current state of model coefficients."""

        # Update recent cost values for training and (if provided) validation sets.
        self.cost_train = self._cost(self._X, self._y)
        if self._compare_to_validation_set:
            self.cost_validation = self._cost(self._X_val, self._y_val)

        # Update handler with cost value for last epoch (currently a stub).
        self._handler.update(self.cost_validation)

    def _report_metrics(self):
        """Print metrics to console."""
        print(f"Epoch {self._epoch_number}:", end=':  ')
        print(f"loss", end="  ")
        print(f"train = {self.cost_train:.10f}", end="  ")
        if self._compare_to_validation_set:
            print(f"validation = {self.cost_validation:.10f}", end="  ")
        print()

    def _plot_metrics(self):
        """Update learning curve plot for current epoch."""

        # Startup: perform start operations after first epoch.
        if self._epoch_number == 1:
            self.plotter = RealTimePlotter(
                parameters=["train loss", "validation loss"],
                num_updates=self.hyper.epochs
            )
            self.plotter.start(
                xlabel="Epoch",
                ylabel="Loss",
                title="Learning curves"
            )

        # Update: add current epoch to learning curve plot.
        self.plotter.update(self.cost_train, self.cost_validation)

    def fit(self, X, y, X_val=None, y_val=None):
        """
        Fit a binary classification model to the training set given by X, y using batch gradient descent.

        :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
        :param np.ndarray y: class labels vector (ground truth for each sample).
        :param np.ndarray X_val: validation set of samples to tune the hyperparameters on (rows-samples, cols-features).
        :param np.ndarray y_val: validation class labels vector (ground truth for each sample).
        """

        # If learning rate of validation set needs to be monitored, make sure validation set is provided.
        if self.reduce_on_plateau:
            assert isinstance(X_val, np.ndarray) and isinstance(y_val, np.ndarray)

        # If a validation set is provided, save it (add bias column) and turn on the compare-to-validation flag.
        if isinstance(X_val, np.ndarray) and isinstance(y_val, np.ndarray):
            self._compare_to_validation_set = True
            self._X_val = np.append(np.ones((X_val.shape[0], 1)), X_val, axis=1)
            self._y_val = y_val

        # Save samples and class labels for training set (add bias column).
        self._X = np.append(np.ones((X.shape[0], 1)), X, axis=1)
        self._y = y

        # Run batch gradient descent optimization.
        self._run_batch_gradient_descent()

        # Return self to support chaining syntax.
        return self

    def predict(self, X):
        """
        Predict class labels {0, 1} = {False, True} on the input samples using the model coefficients and threshold
        parameter. Does not return the probability measures - just the class labels. Adds a bias column to X.

        :param np.ndarray X: set of samples you apply the model on (rows-samples, cols-features).
        :return np.ndarray: model's prediction on the sample set.
        """
        return self._hypothesis(np.append(np.ones((X.shape[0], 1)), X, axis=1)) > self.hyper.threshold

    @property
    def coefficients(self):
        """Return model coefficients."""
        return self._theta
